# Panoramax Command Line Interface

The Panoramax _CLI_ (command-line interface) allows you to upload your local pictures to any Panoramax instance. The CLI is available as a Windows or Linux executable, as well as a Python library for any OS.

The CLI handles classic images with EXIF metadata, as well as images with external CSV metadata.

!!! note

    If you're looking for a simpler way to upload pictures, you could also use the website of any Panoramax instance (which offers a web upload page). The CLI is a better choice however if:

    - You have a __lot__ of pictures to upload
    - You want to automate your uploads
    - You're not afraid of :octicons-terminal-16: __terminals__ 😜
