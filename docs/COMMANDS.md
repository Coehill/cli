# `geovisio`

GeoVisio command-line client (v0.3.13)

**Usage**:

```console
$ geovisio [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--version`: Show GeoVisio command-line client version and exit
* `--install-completion`: Install completion for the current shell.
* `--show-completion`: Show completion for the current shell, to copy it or customize the installation.
* `--help`: Show this message and exit.

**Commands**:

* `collection-status`: Print the status of a collection.
* `download`: Downloads one or many sequences.
* `login`: Authenticate to the given instance.
* `test-process`: (Testing) Preview an upload.
* `upload`: Uploads some files.

## `geovisio collection-status`

Print the status of a collection.

Either a _--location_ option should be provided, with the full location url of the collection
or only the _--id_ combined with the _--api-url_

**Usage**:

```console
$ geovisio collection-status [OPTIONS]
```

**Options**:

* `--id TEXT`: Id of the collection
* `--api-url TEXT`: GeoVisio endpoint URL
* `--location TEXT`: Full url of the collection
* `--wait / --no-wait`: Wait for all pictures to be ready  [default: no-wait]
* `--disable-cert-check / --enable-cert-check`: Disable SSL certificates checks while uploading. This should not be used unless you __really__ know what you are doing.  [default: enable-cert-check]
* `--help`: Show this message and exit.

## `geovisio download`

Downloads one or many sequences.

**Usage**:

```console
$ geovisio download [OPTIONS]
```

**Options**:

* `--api-url TEXT`: GeoVisio endpoint URL  [required]
* `--collection TEXT`: Collection ID. Either use --collection or --user depending on your needs.
* `--user TEXT`: User ID, to get all collections from this user. Either use --collection or --user depending on your needs. The special value 'me' can be provided to get you own sequences, if you're logged in.
* `--path PATH`: Folder where to store downloaded collections.  [default: .]
* `--picture-download-timeout FLOAT`: Timeout time to receive the first byte of the response for each picture upload (in seconds)  [default: 60.0]
* `--disable-cert-check / --enable-cert-check`: Disable SSL certificates checks while downloading. This should not be used unless you __really__ know what you are doing.  [default: enable-cert-check]
* `--file-name [original-name|id]`: Strategy used for naming your downloaded pictures. Either by 'original-name' (by default) or by 'id' in Panoramax server.  [default: original-name]
* `--token TEXT`: GeoVisio token if the geovisio instance needs it. If none is provided and the geovisio instance requires it, the token will be asked during run. Note: is is advised to wait for prompt without using this variable.
* `--external-metadata-dir-name PATH`: Name of the folder where to store Panoramax API responses corresponding to each picture. 
This folder will be created in the pictures folder (so you can use '.' to download pictures in the same folder as the pictures).
If not provided, the API responses will not be persisted.
* `--quality [sd|hd|thumb]`: Quality of the pictures to download. Either 'hd', 'sd', 'thumbnail'. Choosing a lower quality will reduce the download time.  [default: hd]
* `--help`: Show this message and exit.

## `geovisio login`

Authenticate to the given instance.

This will generate credentials and ask the user to visit a page to associate those credentials to the user's account.

The credentials will be stored in /home/a_user/.config/geovisio/config.toml

**Usage**:

```console
$ geovisio login [OPTIONS]
```

**Options**:

* `--api-url TEXT`: GeoVisio endpoint URL  [required]
* `--disable-cert-check / --enable-cert-check`: Disable SSL certificates checks while uploading. This should not be used unless you __really__ know what you are doing.  [default: enable-cert-check]
* `--help`: Show this message and exit.

## `geovisio test-process`

(Testing) Preview an upload.

Generates a TOML file with metadata used for upload. This can be useful to check if all is correct before uploading.

**Usage**:

```console
$ geovisio test-process [OPTIONS] PATH
```

**Arguments**:

* `PATH`: Local path to your sequence folder  [required]

**Options**:

* `--title TEXT`: Collection title. If not provided, the title will be the directory name.
* `--sort-method [filename-asc|filename-desc|time-asc|time-desc]`: Strategy used for sorting your pictures. Either by filename or EXIF time, in ascending or descending order.  [default: time-asc]
* `--split-distance INTEGER`: Maximum distance between two pictures to be considered in the same sequence (in meters).  [default: 100]
* `--split-time INTEGER`: Maximum time interval between two pictures to be considered in the same sequence (in seconds).  [default: 60]
* `--duplicate-distance FLOAT`: Maximum distance between two pictures to be considered as duplicates (in meters).  [default: 1]
* `--duplicate-rotation INTEGER`: Maximum angle of rotation for two too-close-pictures to be considered as duplicates (in degrees).  [default: 30]
* `--help`: Show this message and exit.

## `geovisio upload`

Uploads some files.

Upload some files to a GeoVisio instance. The files will be associated to one or many sequences.

**Usage**:

```console
$ geovisio upload [OPTIONS] PATH
```

**Arguments**:

* `PATH`: Local path to your sequence folder  [required]

**Options**:

* `--api-url TEXT`: GeoVisio endpoint URL  [required]
* `--wait / --no-wait`: Wait for all pictures to be ready  [default: no-wait]
* `--is-blurred / --is-not-blurred`: Define if sequence is already blurred or not  [default: is-not-blurred]
* `--title TEXT`: Collection title. If not provided, the title will be the directory name.
* `--token TEXT`: GeoVisio token if the geovisio instance needs it. If none is provided and the geovisio instance requires it, the token will be asked during run. Note: is is advised to wait for prompt without using this variable.
* `--sort-method [filename-asc|filename-desc|time-asc|time-desc]`: Strategy used for sorting your pictures. Either by filename or EXIF time, in ascending or descending order.  [default: time-asc]
* `--split-distance INTEGER`: Maximum distance between two pictures to be considered in the same sequence (in meters).  [default: 100]
* `--split-time INTEGER`: Maximum time interval between two pictures to be considered in the same sequence (in seconds).  [default: 60]
* `--duplicate-distance FLOAT`: Maximum distance between two pictures to be considered as duplicates (in meters).  [default: 1]
* `--duplicate-rotation INTEGER`: Maximum angle of rotation for two too-close-pictures to be considered as duplicates (in degrees).  [default: 30]
* `--picture-upload-timeout FLOAT`: Timeout time to receive the first byte of the response for each picture upload (in seconds)  [default: 60.0]
* `--disable-cert-check / --enable-cert-check`: Disable SSL certificates checks while uploading. This should not be used unless you __really__ know what you are doing.  [default: enable-cert-check]
* `--help`: Show this message and exit.
