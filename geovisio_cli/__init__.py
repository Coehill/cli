"""
Geovio client cli tool
"""
__version__ = "0.3.13"

USER_AGENT = f"geovisio_cli_{__version__}"
