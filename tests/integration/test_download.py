import os
import pytest
import requests
import geovisio_cli.exception
import geovisio_cli.model
import geovisio_cli.sequences.status
import geovisio_cli.sequences.upload
import geovisio_cli.download
from tests.conftest import FIXTURE_DIR
from pathlib import Path
import shutil

from tests.integration.conftest import cleanup_geovisio


@pytest.mark.datafiles(
    os.path.join(FIXTURE_DIR, "e1.jpg"),
    os.path.join(FIXTURE_DIR, "e2.jpg"),
    os.path.join(FIXTURE_DIR, "e3.jpg"),
)
def test_valid_collection_download(geovisio_with_token, datafiles, tmp_path):
    uploadReports = geovisio_cli.sequences.upload.upload(
        path=Path(datafiles),
        geovisio=geovisio_with_token,
        title="some title",
        pictureUploadTimeout=20,
        wait=True,
    )
    assert len(uploadReports[0].uploaded_pictures) == 3

    col_id = uploadReports[0].location.split("/")[-1]

    with requests.session() as s:
        geovisio_cli.download.download_collection(
            session=s,
            collection_id=col_id,
            geovisio=geovisio_with_token,
            path=tmp_path / "dl",
            file_name="original-name",
        )
    path1 = tmp_path / "dl" / "some_title" / "e1.jpg"
    assert path1.exists()
    path2 = tmp_path / "dl" / "some_title" / "e2.jpg"
    assert path2.exists()
    path3 = tmp_path / "dl" / "some_title" / "e3.jpg"
    assert path3.exists()


@pytest.fixture(scope="module")
def load_data(geovisio_with_token, tmpdir_factory):
    import tempfile

    # do not use tmp_path since we want to use it as a fixture module
    tmp_dir = Path(tempfile.gettempdir())
    fixture_dir = Path(FIXTURE_DIR)

    tmp_dir = Path(tmpdir_factory.mktemp("geovisio_data"))

    dir1 = tmp_dir / "col1"
    dir1.mkdir()
    dir2 = tmp_dir / "col2"
    dir2.mkdir()
    shutil.copy(fixture_dir / "e1.jpg", dir1 / "e1.jpg")
    shutil.copy(fixture_dir / "e2.jpg", dir1 / "e2.jpg")
    shutil.copy(fixture_dir / "e3.jpg", dir2 / "e3.jpg")
    cleanup_geovisio(geovisio_with_token)

    uploadReports1 = geovisio_cli.sequences.upload.upload(
        path=dir1,
        geovisio=geovisio_with_token,
        title="collection1",
        pictureUploadTimeout=20,
        wait=True,
    )
    assert len(uploadReports1[0].uploaded_pictures) == 2
    uploadReports2 = geovisio_cli.sequences.upload.upload(
        path=dir2,
        geovisio=geovisio_with_token,
        title="collection2",
        pictureUploadTimeout=20,
        wait=True,
    )
    assert len(uploadReports2[0].uploaded_pictures) == 1
    return [uploadReports1, uploadReports2]


def test_valid_user_me_download(geovisio_with_token, tmp_path, load_data):

    # dir is empty at first
    files = sorted((tmp_path / "dl").rglob("*"))
    assert files == []

    geovisio_cli.download.download(
        user="me",
        geovisio=geovisio_with_token,
        path=tmp_path / "dl",
        file_name="original-name",
    )
    files = sorted((tmp_path / "dl").rglob("*"))
    # we did not ask for external metadata, so no external metadata should be downloaded
    assert files == sorted(
        [
            tmp_path / "dl" / "collection1",
            tmp_path / "dl" / "collection1" / "e1.jpg",
            tmp_path / "dl" / "collection1" / "e2.jpg",
            tmp_path / "dl" / "collection2",
            tmp_path / "dl" / "collection2" / "e3.jpg",
        ]
    ), files
    e1_write_time = os.path.getmtime(tmp_path / "dl" / "collection1" / "e1.jpg")
    e2_write_time = os.path.getmtime(tmp_path / "dl" / "collection1" / "e2.jpg")
    e3_write_time = os.path.getmtime(tmp_path / "dl" / "collection2" / "e3.jpg")
    # if we ask the same download, we should have some external metadata, and no data should have been downloaded
    geovisio_cli.download.download(
        user="me",
        geovisio=geovisio_with_token,
        path=tmp_path / "dl",
        file_name="original-name",
        external_metadata_dir_name="external_metadata",
    )
    files = sorted((tmp_path / "dl").rglob("*"))
    assert files == sorted(
        [
            tmp_path / "dl" / "collection1",
            tmp_path / "dl" / "collection1" / "e1.jpg",
            tmp_path / "dl" / "collection1" / "external_metadata",
            tmp_path / "dl" / "collection1" / "external_metadata" / "e1.jpg.json",
            tmp_path / "dl" / "collection1" / "e2.jpg",
            tmp_path / "dl" / "collection1" / "external_metadata" / "e2.jpg.json",
            tmp_path / "dl" / "collection2",
            tmp_path / "dl" / "collection2" / "e3.jpg",
            tmp_path / "dl" / "collection2" / "external_metadata",
            tmp_path / "dl" / "collection2" / "external_metadata" / "e3.jpg.json",
        ]
    ), files
    assert (tmp_path / "dl" / "collection1" / "e1.jpg").exists()
    assert (tmp_path / "dl" / "collection1" / "e2.jpg").exists()
    assert (tmp_path / "dl" / "collection2" / "e3.jpg").exists()

    # since the picture was already downloaded, the download was skipped
    e1_write_time_after = os.path.getmtime(tmp_path / "dl" / "collection1" / "e1.jpg")
    e2_write_time_after = os.path.getmtime(tmp_path / "dl" / "collection1" / "e2.jpg")
    e3_write_time_after = os.path.getmtime(tmp_path / "dl" / "collection2" / "e3.jpg")
    assert e1_write_time == e1_write_time_after
    assert e2_write_time == e2_write_time_after
    assert e3_write_time == e3_write_time_after


def test_invalid_user_id_download(geovisio_with_token, tmp_path, load_data):
    with pytest.raises(geovisio_cli.exception.CliException) as e:
        geovisio_cli.download.download(
            user="pouet",
            geovisio=geovisio_with_token,
            path=tmp_path / "dl",
            file_name="original-name",
        )
    assert str(e.value).startswith("Impossible to find user pouet")


def test_invalid_collection_id_download(geovisio_with_token, tmp_path, load_data):
    with pytest.raises(geovisio_cli.exception.CliException) as e:
        geovisio_cli.download.download(
            collection="pouet",
            geovisio=geovisio_with_token,
            path=tmp_path / "dl",
            file_name="original-name",
        )
    assert str(e.value).startswith("Impossible to get collection pouet"), e.value


def test_valid_user_id_download(geovisio_with_token, tmp_path, load_data):
    user = requests.get(f"{geovisio_with_token.url}/api/users")
    user.raise_for_status()
    user_id = next(u["id"] for u in user.json()["users"] if u["name"] == "elysee")

    # we can download even without a token
    geovisio_cli.download.download(
        user=user_id,
        geovisio=geovisio_cli.model.Geovisio(url=geovisio_with_token.url),
        path=tmp_path / "dl",
        file_name="id",
    )
    # if we ask by id, we got the id as file_name
    pic1_id = load_data[0][0].uploaded_pictures[0].id
    pic2_id = load_data[0][0].uploaded_pictures[1].id
    pic3_id = load_data[1][0].uploaded_pictures[0].id
    assert (tmp_path / "dl" / "collection1" / f"{pic1_id}.jpg").exists()
    assert (tmp_path / "dl" / "collection1" / f"{pic2_id}.jpg").exists()
    assert (tmp_path / "dl" / "collection2" / f"{pic3_id}.jpg").exists()

    # we can also download with a lower quality
    geovisio_cli.download.download(
        user=user_id,
        geovisio=geovisio_cli.model.Geovisio(url=geovisio_with_token.url),
        path=tmp_path / "dl_sd",
        file_name="id",
        quality=geovisio_cli.download.Quality.sd,
    )
    assert (tmp_path / "dl_sd" / "collection1" / f"{pic1_id}.jpg").exists()
    assert (tmp_path / "dl_sd" / "collection1" / f"{pic2_id}.jpg").exists()
    assert (tmp_path / "dl_sd" / "collection2" / f"{pic3_id}.jpg").exists()

    geovisio_cli.download.download(
        user=user_id,
        geovisio=geovisio_cli.model.Geovisio(url=geovisio_with_token.url),
        path=tmp_path / "dl_thumb",
        file_name="id",
        quality=geovisio_cli.download.Quality.thumb,
    )

    assert (tmp_path / "dl_thumb" / "collection1" / f"{pic1_id}.jpg").exists()
    assert (tmp_path / "dl_thumb" / "collection1" / f"{pic2_id}.jpg").exists()
    assert (tmp_path / "dl_thumb" / "collection2" / f"{pic3_id}.jpg").exists()

    size = lambda p: os.path.getsize(p)

    for c, f in [
        ("collection1", f"{pic1_id}.jpg"),
        ("collection1", f"{pic2_id}.jpg"),
        ("collection2", f"{pic3_id}.jpg"),
    ]:
        assert size(tmp_path / "dl_thumb" / c / f) <= size(tmp_path / "dl_sd" / c / f)
        assert size(tmp_path / "dl_sd" / c / f) <= size(tmp_path / "dl" / c / f)
